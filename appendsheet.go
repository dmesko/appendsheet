package main

// client secret = XXS-YBriml0Wmpwhp8gotjay
// client id = 880094395360-pnp8vke2ubj0vk1ne03d8cb24vbad9ml.apps.googleusercontent.com

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
)

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func main() {
	user := flag.String("user", "", "ID/name of the user to append.")
	item := flag.String("item", "", "ID/name of the item, which user is buying.")
	sheet := flag.String("sheet", "", "Google Spreadsheet ID to append to.")
	flag.Parse()

	if *user == "" || *item == "" {
		fmt.Println("Both User and Item are required parameters")
		flag.Usage()
		os.Exit(1)
	}

	if *sheet == "" {
		*sheet = "1mFwPRE6dXeNWbhJzYd8Ms0ZB4aAyVfcsLzZnzeI7f4w"
	}

	b, err := ioutil.ReadFile("sheet-credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)

	sheetsService, err := sheets.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Sheets client: %v", err)
	}

	// https://docs.google.com/spreadsheets/d/1mFwPRE6dXeNWbhJzYd8Ms0ZB4aAyVfcsLzZnzeI7f4w/edit#gid=0
	spreadsheetID := *sheet
	tableRange := "Actions!A1:C"

	// How the input data should be interpreted.
	valueInputOption := "USER_ENTERED"

	// How the input data should be inserted.
	insertDataOption := "OVERWRITE"

	rawTime := time.Now()
	niceTime := rawTime.Format("2006-01-02 15:04:05")

	rb := &sheets.ValueRange{
		MajorDimension: "ROWS",
		Range:          tableRange,
		Values: [][]interface{}{
			{niceTime, user, item},
		},
	}

	_, err = sheetsService.Spreadsheets.Values.Append(spreadsheetID, tableRange, rb).ValueInputOption(valueInputOption).InsertDataOption(insertDataOption).Do()
	if err != nil {
		log.Fatalf("Unable to append data to sheet: %v", err)
	}

}
