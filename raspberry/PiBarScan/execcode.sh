#!/bin/ksh

CONFIG=barcode.conf

TIMEOUT_SEC=10

GREEN_PIN=17
RED_PIN=18

#state="RUSER"
user=""
item=""
userReadSeconds=-1

function readUser {
	killGreenTimeout

	if [[ $BARCODE != P* ]]
	then
		echo "Wrong User Code: ${BARCODE}"
		redError &
		return
	fi

	user=$BARCODE
	item=""

	startGreenWithTimeout
	userReadSeconds=$SECONDS

	echo "User: ${user}"
}

function readItem {
	barcodeLength=${#BARCODE}
	echo $barcodeLength
	if [[ $BARCODE != I* ]] && [ $barcodeLength -ne 13 ]
	then
		echo "Wrong Item Code: ${BARCODE}"
		return
	fi

	item=$BARCODE
	echo "Loaded item: ${item} for user: ${user}"

	killGreenTimeout
	greenOff

	echo "going to upload data"
	./appendsheet -user "*${user}*" -item "*${item}*"
	res=$?
	if [ $res -eq 0 ]
	then
		echo "data uploaded"
		greenBlick
	else
		echo "upload error, status: ${res}"
		redError
	fi

	user=""
	item=""

}

greenTimeoutPid=-1
function greenTimeout {
	sleep ${TIMEOUT_SEC}
	echo "green diode times out"
	gpio -g write ${GREEN_PIN} 0
}

function startGreenWithTimeout {
	greenOn
	greenTimeout &
	greenTimeoutPid=$!
	echo "Green LED started with timeout job PID=${greenTimeoutPid}"
}

function killGreenTimeout {
	if [ $greenTimeoutPid -ne -1 ]
	then
		kill -HUP ${greenTimeoutPid}
		echo "Green timeout job killed."
		greenTimeoutPid=-1
	fi
}

function redError {
	for i in {1..10}
	do
		redOn
		sleep 0.2
		redOff
		sleep 0.1
	done
}

function greenBlick {
	for i in {1..10}
	do
		greenOn
		sleep 0.1
		greenOff
		sleep 0.1
	done
}

function greenOn {
	gpio -g write ${GREEN_PIN} 1
}

function greenOff {
	gpio -g write ${GREEN_PIN} 0
}

function redOn {
	gpio -g write ${RED_PIN} 1
}

function redOff {
	gpio -g write ${RED_PIN} 0
}

gpio -g mode ${GREEN_PIN} out
gpio -g mode ${RED_PIN} out
greenOff
redOff

while read BARCODE
do
	#while IFS='|' read Regex Action
	#do
	#	[[ $Regex == \#* ]] && continue
	#	[[ $BARCODE == $Regex ]] && eval $Action
	#done <$CONFIG

	if [[ $BARCODE == P* ]]
	then
		readUser
	else
		rItemDuration=$( echo "${SECONDS}-${userReadSeconds}" | bc )
		echo "Item read after ${rItemDuration} seconds"
		if [[ "$user" == "" ]] || (( $(echo "${rItemDuration}>${TIMEOUT_SEC}" | bc -l) ))
		then
			# call read user, it is after timeout!
			echo "item load was made after ${rItemDuration} seconds. Treating it as another user read."
			readUser
		else
			readItem
		fi
	fi

done
